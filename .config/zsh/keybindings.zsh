# CTRL + RIGHT_ARROW (go forward entire word)
bindkey "^[[1;5C" forward-word

# CTRL + LEFT_ARROW (go back entire word)
bindkey "^[[1;5D" backward-word

# CTRL + DELETE (remove entire word after cursor)
bindkey -M emacs "^[[3;5~" kill-word

# CTRL + BACKSPACE (remove entire word in front of cursor)
bindkey '^H' backward-kill-word
